# -*- coding: utf-8 -*-
try:
    from geopy.distance import vincenty
    from geopy.distance import VincentyDistance
    from geopy.distance import Point
except ImportError:
    geopy_info = False
else:
    geopy_info = True

#https://stackoverflow.com/a/20677983
def line_intersection(line1, line2):
    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])
    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]
    div = det(xdiff, ydiff)
    if div == 0:
       raise Exception('lines do not intersect')
    d = (det(*line1), det(*line2))
    x = det(d, xdiff) / div
    y = det(d, ydiff) / div
    dist_line1 = vincenty((line1[0]), (x,y)).kilometers
    dist_line2 = vincenty((line2[0]), (x,y)).kilometers
    return x, y

#https://stackoverflow.com/a/9997374
def ccw(A,B,C):
	return (C[1]-A[1])*(B[0]-A[0]) > (B[1]-A[1])*(C[0]-A[0])
def intersect(A,B,C,D):
	return ccw(A,C,D) != ccw(B,C,D) and ccw(A,B,C) != ccw(A,B,D)

def destination_point(lat1, lon1, d, b):
    """ b en ° , d en km """
    origin = Point(lat1, lon1)
    if b == '': return False
    b = float(b.replace(',', '.'))
    destination = VincentyDistance(kilometers=d).destination(origin, b)
    lat2, lon2 = destination.latitude, destination.longitude
    return (lat2, lon2)

def get_line(sup, asup, distanceM, distancem, dictionnaire):
    lat1 = dictionnaire[sup].lat
    lon1 = dictionnaire[sup].lon
    destM = destination_point(lat1, lon1, distanceM, asup)
    if distancem :
        destm = destination_point(lat1, lon1, (distancem), asup)
    else :
        destm = False
    if destM :
        lat2, lon2 = destM
        if destm :
            lat1, lon1 = destm
        result = [[lat1, lon1],[lat2, lon2]]
    else:
        result = [[0,0],[0,0]]
    return result

def liste_voisin(listeG, distanceGM, distanceGm, distanceP, dictionnaire,l_support_GsmP_limitrof, GsmR, GsmP, exploitants, opt_azimut, opt_faisceau):
    def recherche_azimut():
        azimutG = dictionnaire[supportG].getAzimut(GsmR, ['SNCF Réseau'])
        azimutP = dictionnaire[supportP].getAzimut(GsmP, exploitants)
        if opt_faisceau:
            liste=[]
            for aG in azimutG:
                liste.append(str(float(aG.replace(',','.'))+opt_faisceau))
                liste.append(str(float(aG.replace(',','.'))-opt_faisceau))
                #liste.append(aG+opt_faisceau)
            azimutG=liste
        result = False
        for aG in azimutG:
            for aP in azimutP:
                lineG = get_line(supportG, aG, distanceGM, distanceGm, dictionnaire)
                lineP = get_line(supportP, aP, distanceP, False, dictionnaire)
                if intersect(lineG[0],lineG[1],lineP[0],lineP[1]):
                    d_azimut['gsmP'].append(lineP)
                    d_azimut['gsmR'].append(lineG)
                    result = True
        return result

    d_voisin = {}
    d_azimut = {'gsmR':[], 'gsmP':[]}
    for supportG in listeG :
        lat1 = dictionnaire[supportG].lat
        lon1 = dictionnaire[supportG].lon
        origin = Point(lat1, lon1)
        limit0 = VincentyDistance(kilometers=distanceGM).destination(origin, 0).format_decimal()
        limit90 = VincentyDistance(kilometers=distanceGM).destination(origin, 90).format_decimal()
        lat0, lon0 = limit0.split(', ')
        lat90, lon90 = limit90.split(', ')
        dif0 = float(lat0) - lat1
        dif90 = float(lon90) - lon1
        latmini = lat1 - dif0
        latmaxi = float(lat0)
        lonmini = lon1 - dif90
        lonmaxi = float(lon90)
        for supportP in  l_support_GsmP_limitrof:
            lat2 = dictionnaire[supportP].lat
            lon2 = dictionnaire[supportP].lon
            if latmini <= lat2 <= latmaxi and lonmini <= lon2 <= lonmaxi:
                if opt_azimut:
                    azimut = recherche_azimut()
                    if azimut:
                        d_voisin.setdefault(supportG,[]).append(supportP)
                else:
                    d_voisin.setdefault(supportG,[]).append(supportP)
    return d_voisin, d_azimut
