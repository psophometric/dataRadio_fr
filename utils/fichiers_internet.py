# -*- coding: utf-8 -*-
import re
import urllib
import zipfile
import os

# https://www.python.org/dev/peps/pep-0476/
if "nt" == os.name:
    print "OS detect : Windows"
    import ssl
    try:
        context = ssl._create_unverified_context()
        urllib.urlopen("https://www.data.gouv.fr/fr/datasets/donnees-sur-les-installations-radioelectriques-de-plus-de-5-watts-1/", context=context)
    except:
        pass


def verifier_site(anfr_data):
    """Avec une expression régulière sur la page web du jeu de données,
    récupère le nom du fichier (DATA et Tables_de_reference) le plus récent,
    ainsi que son URL"""
    page=urllib.urlopen(anfr_data)
    strpage=page.read()
    redatalink = r'https?://(?:[a-z0-9\-]+\.)+[a-z]{2,6}(?:/[^/#?]+)+\DATA.zip'
    redatafile = r'[0-9]+_DATA.zip'
    rereflink = r'https?://(?:[a-z0-9\-]+\.)+[a-z]{2,6}(?:/[^/#?]+)+\_Tables_de_reference.zip'
    rereffile = r'[0-9]+_Tables_de_reference.zip'
    datalinkre = re.compile(redatalink,re.IGNORECASE)
    datalink = datalinkre.findall(strpage)
    datazipre = re.compile(redatafile,re.IGNORECASE)
    datazip = datazipre.findall(strpage)
    reflinkre = re.compile(rereflink,re.IGNORECASE)
    reflink = reflinkre.findall(strpage)
    refzipre = re.compile(rereffile,re.IGNORECASE)
    refzip = refzipre.findall(strpage)
    print "Fichier disponible sur internet :", datazip[0]
    print "Fichier disponible sur internet :", refzip[0]
    return datalink[0], datazip[0], reflink[0], refzip[0]

def telechargement(repertoire_zip, link, file):
    """Télécharge les fichiers DATA et Tables_de_reference"""
    try:
        ls_dir_zip = os.listdir(repertoire_zip)
    except:
        os.mkdir(repertoire_zip)
    urllib.urlretrieve(link, repertoire_zip + file)
    return True

def unzip(source_file, source_dir, destination_dir):
    """Extraction du fichier zip"""
    with zipfile.ZipFile(source_dir + source_file) as zf:
        zf.extractall(destination_dir)
        print "Extraction finie"

def insee(dirname, insee_file, insee_link):
    """Vérifie la présence du jeu de données INSEE, sinon le télécharge"""
    if os.path.isfile(dirname + insee_file):
        print "Fichier INSEE présent dans le répertoire csv/"
    else :
        print "Fichier INSEE absent dans le répertoire csv/"
        if "nt" == os.name:
            context = ssl._create_unverified_context()
            urllib.urlretrieve(insee_link, dirname + insee_file, context=context)
        else:
            urllib.urlretrieve(insee_link, dirname + insee_file)


def supprimer_repertoire(repertoire):
    """ Supprime le répertoire 'repertoire'"""
    #SUPRESSION DES FICHIERS DU REPERTOIRE
    for root, dirs, files in os.walk(repertoire, topdown=False):
        for name in files:
            os.remove(os.path.join(root, name))
        for name in dirs:
            os.rmdir(os.path.join(root, name))
    #SUPRESSION DU REPERTOIRE
    os.removedirs(repertoire)
    return True
