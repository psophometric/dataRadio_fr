# -*- coding: utf-8 -*-
from __future__ import unicode_literals #str utf-8
import csv
from classes import Support, Antenne, Station, Emetteur, Bande

def chargement_fichier_complexe(chemin,i, j):
    """Retourne un dictionnaire au format i:[j]"""
    fichier = open(chemin, "rb")
    liste = list(csv.reader(fichier,delimiter=b';'))
    liste.pop(0) #suppression 1ere ligne
    fichier.close()
    dictionnaire = {}
    for ligne in liste :
        dictionnaire.setdefault(ligne[i],[]).append(ligne[j])
    return dictionnaire

def chargement_fichier_simple(chemin,i):
    """Retourne un dictionnaire au format i:[0:]"""
    fichier = open(chemin, "rb")
    liste = list(csv.reader(fichier,delimiter=b';'))
    liste.pop(0) #suppression 1ere ligne
    fichier.close()
    dictionnaire = {}
    for ligne in liste :
        dictionnaire[ligne[i]] = ligne[0:]
    return dictionnaire

def csv2dict_objets(repertoire):
    def code_insee2postal(n_insee):
        """Retourne une liste au format [Nom commune,n°département]
            récupérée dans le dictionnaire d_insee
            selon le 'n_insee' indiqué"""
        commune = d_insee.get(n_insee)
        if not commune: # Pour gerer les erreurs Insee
            return ['na','na']
        else:
            return [commune[1],n_insee[0:2]]
    def conversion_coord(coord_DMS) :
        """Converti les coordonnées géographiques DMS en DD"""
        if coord_DMS[3] == "N" or coord_DMS[3] == "E":
            coord_DD = float(coord_DMS[0]) + (float(coord_DMS[1]) * 1/60) + (float(coord_DMS[2]) * 1/3600)
        if coord_DMS[3] == "S" or coord_DMS[3] == "W":
            coord_DD = (float(coord_DMS[0]) + (float(coord_DMS[1]) * 1/60) + (float(coord_DMS[2]) * 1/3600)
                       )*(-1)
        return coord_DD

    try:
        d_station2support=chargement_fichier_complexe(repertoire+"/SUP_SUPPORT.txt", 1, 0)
        d_antenne2station=chargement_fichier_complexe(repertoire+"/SUP_ANTENNE.txt", 1, 0)
        d_antenne2emetteur=chargement_fichier_complexe(repertoire+"/SUP_EMETTEUR.txt", 3, 0)
        d_emetteur2bande=chargement_fichier_complexe(repertoire+"/SUP_BANDE.txt", 2, 1)

        d_antenne=chargement_fichier_simple(repertoire+"/SUP_ANTENNE.txt", 1)
        d_support=chargement_fichier_simple(repertoire+"/SUP_SUPPORT.txt", 0)
        d_station=chargement_fichier_simple(repertoire+"/SUP_STATION.txt", 0)
        d_emetteur=chargement_fichier_simple(repertoire+"/SUP_EMETTEUR.txt", 0)
        d_bande=chargement_fichier_simple(repertoire+"/SUP_BANDE.txt", 1)

        d_exploitant=chargement_fichier_simple(repertoire+"/SUP_EXPLOITANT.txt", 0)
        d_nature=chargement_fichier_simple(repertoire+"/SUP_NATURE.txt", 0)
        d_proprio=chargement_fichier_simple(repertoire+"/SUP_PROPRIETAIRE.txt", 0)
        d_type_antenne=chargement_fichier_simple(repertoire+"/SUP_TYPE_ANTENNE.txt", 0)
        d_insee=chargement_fichier_simple(repertoire+"/code-insee.csv", 0)
    except :
        return False
    dictionnaire = {}
    for antenne in d_antenne.values(): #parcours du fichier antenne
        try:
            supports = d_station2support[antenne[0]] #recherche supports
            station = d_station[antenne[0]]
            emetteurs = d_antenne2emetteur[antenne[1]] # recherche emetteurs
        except:
            pass #antenne sans support, sans station ou sans emetteur
        else:
            for support in supports:   # parcours des supports, cas de plusieurs supports...

                o_ant = Antenne(type = d_type_antenne[antenne[2]][1].decode('latin-1'),
                dimension = antenne[3], rayon = antenne[4],
                azimut = antenne[5], altitude = antenne[6])            # creation de l'objet antenne

                id_exploitant, trash = station[1].split(',')
                comsis_station, trash = station[2].split(',')

                o_sta = Station(id = station[0],
                                exploitant = d_exploitant[id_exploitant][1].decode('latin-1'),
                                comsis = comsis_station,
                                dateImplan = station[3],
                                dateModif = station[4],
                                dateService = station[5])           # creation de l'objet station
                for emetteur in emetteurs:
                    data_emetteur = d_emetteur[emetteur]
                    o_eme = Emetteur(systeme = data_emetteur[1]) # creation de l'objet emetteur
                    try:
                        bandes = d_emetteur2bande[data_emetteur[0]] # recherche bandes
                        for bande in bandes:
                            data_bande = d_bande[bande]
                            o_bande = Bande(debut = data_bande[3],
                            fin = data_bande[4],
                            unite = data_bande[5]) # creation de l'objet bande
                            o_eme.bandes.append(o_bande)
                        o_sta.emetteurs.append(o_eme)
                    except:
                        pass #emetteur sans bande...

                if dictionnaire.has_key(support):   # Si le dictionnaire existe deja ?
                    o_sup = dictionnaire[support]   # recupération de l'objet support existant
                    mutu = 0
                    for ant in o_sup.antennes :     # parcours des antennes du support, cas du mutualisation d'antennes...
                        if ant.azimut == o_ant.azimut and ant.altitude == o_ant.altitude : # Mutualisation détectée
                            for sta in ant.stations :
                                if sta.id != o_sta.id: # Protection contre duplication d'antenne sur les stations sur plusieurs supports.
                                    ant.stations.append(o_sta)
                                    mutu = 1
                                    break
                    if mutu == 0:
                        o_ant.stations.append(o_sta)
                        o_sup.antennes.append(o_ant) # ajout de l'objet antenne à l'objet support
                else :
                    data_support = d_support[support]
                    support_ville, support_departement = code_insee2postal(data_support[18])
                    try:
                        support_proprietaire = d_proprio[data_support[12]][1].decode('latin-1')
                    except:
                        support_proprietaire = ""
                    o_sup = Support(id = data_support[0],
                    nature = d_nature[data_support[2]][1].decode('latin-1'),
                    code_postal = data_support[17],
                    ville = support_ville, departement = support_departement,
                    lat = conversion_coord(data_support[3:7]),
                    lon = conversion_coord(data_support[7:11]),
                    hauteur = data_support[11],
                    proprietaire = support_proprietaire) #creation de l'objt support
                    o_ant.stations.append(o_sta)
                    o_sup.antennes.append(o_ant) # ajout de l'objet antenne à l'objet support
                    dictionnaire[support]=o_sup

    del d_station2support
    del d_antenne2station
    del d_antenne2emetteur
    del d_emetteur2bande
    del d_antenne
    del d_support
    del d_station
    del d_emetteur
    del d_bande
    del d_exploitant
    del d_nature
    del d_proprio
    del d_type_antenne
    #del d_insee
    return dictionnaire
