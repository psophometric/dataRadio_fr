# -*- coding: utf-8 -*-
import json
from classes import Support, Antenne, Station, Emetteur, Bande

def json2dict_objets(fichier):
    try:
        with open(fichier, 'r') as f:
            data = json.load(f)
    except:
        return False
    dictionnaire = {}
    date = data['date']
    for support in data['supports']:
        o_sup = Support(support['id'], support['nature'],
            support['code_postal'], support['ville'], support['departement'],
            support['lat'], support['lon'],
            support['hauteur'], support['proprietaire'])
        for antenne in support['antennes']:
            o_ant = Antenne(antenne['type'],
                antenne['dimension'], antenne['rayon'],
                antenne['azimut'], antenne['altitude'])
            o_sup.antennes.append(o_ant)
            for station in antenne['stations']:
                o_sta = Station(station['id'], station['exploitant'],
                    station['comsis'], station['dateImplan'],
                    station['dateModif'], station['dateService'])
                o_ant.stations.append(o_sta)
                for emetteur in station['emetteurs']:
                    o_eme = Emetteur(emetteur['systeme'])
                    o_sta.emetteurs.append(o_eme)
                    for bande in emetteur['bandes']:
                        o_ban = Bande(bande['debut'],
                            bande['fin'],
                            bande['unite'])
                        o_eme.bandes.append(o_ban)
        dictionnaire[o_sup.id]=o_sup
    return dictionnaire, date

def dict_objets2json(liste):
    """ Mise en forme du dictionnaire au format json à partir d'une liste """
    d_final = {}
    ### DONNEES
    d_final['supports'] = []
    for support in liste:
        d_final["supports"].append(dictionnaire[support])
    j = json.dumps(d_final, default=lambda o: o.__dict__,
                    sort_keys=True, indent=4)
    return j

def sauvegarde_fichier(dossier, nom_fichier, fichier):
    """ Sauvegarde du 'fichier' à l'emplacement 'dossier'/'nom_fichier'  """
    with open(dossier[1:] + nom_fichier, 'w') as f:
        f.write(fichier)
    return True
