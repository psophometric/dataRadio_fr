# -*- coding: utf-8 -*-
try:
    import folium
except ImportError:
    folium_info = False
else:
    folium_info = True
import webbrowser

def affichage_azimut(map, liste, couleur):
    for coordinates in liste:
        my_PolyLine=folium.PolyLine(locations=coordinates,weight=5, color=couleur)
        map.add_child(my_PolyLine)
    return True

def affichage_support(map, liste, couleur, option_cluster, dictionnaire):
    if option_cluster:
        marker_map = folium.MarkerCluster().add_to(map)
    else:
        marker_map = map
    for support in liste:
        data_sup = dictionnaire[support].print_support().replace('\n','<br>')
        geo=(dictionnaire[support].lat,dictionnaire[support].lon)
        html=folium.IFrame(html=data_sup,width=700, height=300)
        popup = folium.Popup(html, max_width=800)
        folium.RegularPolygonMarker(location=geo, popup=popup,
                   fill_color=couleur, number_of_sides=3, radius=10).add_to(marker_map)
    return True

def creation_map():
    map = folium.Map(location=[46.3630104, 2.9846608], zoom_start=6,
                       tiles='Stamen Toner')
    return map

def sauvegarde_map(map,fichier, option_affiche):
    map.save(fichier)
    if option_affiche:
        webbrowser.open(fichier)
    return True
