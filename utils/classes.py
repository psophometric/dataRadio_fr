# -*- coding: utf-8 -*-
from __future__ import unicode_literals #str utf-8
from collections import OrderedDict

class Support(object):
    __slots__=('id', 'nature',
    'code_postal', 'ville', 'departement',
    'lat', 'lon', 'hauteur', 'proprietaire','antennes')
    def __init__(self, id, nature,
    code_postal, ville, departement,
    lat, lon, hauteur, proprietaire):
        self.id = id
        self.nature = nature
        self.code_postal = code_postal
        self.ville = ville
        self.departement = departement
        self.lat = lat
        self.lon = lon
        self.hauteur = hauteur
        self.proprietaire = proprietaire
        self.antennes = []
    def tri(self, departements, exploitants, systemes):
        if departements :
            if self.departement in departements:
                pass
            else :
                return False
        if exploitants:
            for antenne in self.antennes:
                for station in antenne.stations:
                    if station.exploitant in exploitants:
                        if systemes :
                            for emetteur in station.emetteurs:
                                if emetteur.systeme in systemes:
                                    return True
                        else:
                            return True
        else:
            if systemes:
                for antenne in self.antennes:
                    for station in antenne.stations:
                        for emetteur in station.emetteurs:
                            if emetteur.systeme in systemes:
                                return True
            else:
                return True
        return False
    def toDICT(self):
        dico = {}
        dico['id'] = self.id
        dico['nature'] = self.nature
        dico['hauteur'] = self.hauteur
        dico['proprietaire'] = self.proprietaire
        dico['code_postal'] = self.code_postal
        dico['ville'] = self.ville
        dico['departement'] = self.departement
        dico['lat'] = self.lat
        dico['lon'] = self.lon
        dico['antennes'] = [antenne.toDICT() for antenne in self.antennes]
        return dico
    def get_exploitant_gsm(self):
        liste = []
        l_gsm = ['GSM R', 'GSM 1800', 'GSM 1800/GSM 900', 'GSM 900', 'GSM 900/GSM 1800', 'GSM 900/UMTS 2100',
        'UMTS 2100', 'UMTS 2100/UMTS 900', 'UMTS 900', 'LTE 1800', 'LTE 2600', 'LTE 700', 'LTE 800']
        for antenne in self.antennes:
            for station in antenne.stations:
                for emetteur in station.emetteurs:
                    if emetteur.systeme in l_gsm:
                        liste.append(station.exploitant)
        return set(liste)
    def print_support(self):
        data_str = '{0} {1} ({2})\n'.format(self.id,self.proprietaire,self.ville,self.departement)
        data_str += "{0} ({1}m)\n".format(self.nature, self.hauteur)
        try :
            liste = sorted(self.antennes, key=lambda x: float(x.altitude.replace(',', '.')), reverse=True)
        except:
            liste = self.antennes
        for antenne in liste:
            ant = [antenne.altitude, antenne.azimut]
            tec = ""
            compteur = 0
            for station in antenne.stations :
                exp = station.exploitant
                for emetteur in station.emetteurs:
                    tec += ' ' + emetteur.systeme
                if compteur == 0 :
                    data_str += '{0:5}m ({1:5}°) - {2:20} -{3}\n'.format(ant[0], ant[1], exp, tec)
                else :
                    data_str += '|____________>> - {0:20} -{1:3}\n'.format(exp, tec)
                compteur += 1
        return data_str
    def getAzimut(self, techno, exploitants):
        liste=[]
        for antenne in self.antennes:
            for station in antenne.stations:
                if station.exploitant.encode('utf8') in exploitants: #encode() fix unicode issue on Win10...
                    for emetteur in station.emetteurs:
                        if emetteur.systeme in techno:
                            liste.append(antenne.azimut)
        return set(liste)
    def show_support(self):
        data_str = [self.proprietaire,self.departement]
        return data_str
    def show_antennes(self):
        try:
            liste = sorted(self.antennes, key=lambda x: float(x.altitude.replace(',', '.')), reverse=True)
        except:
            liste = self.antennes
            pass
        dico_ant=OrderedDict()
        for antenne in liste:
            ant = (antenne.altitude, antenne.azimut)
            dico_ant[ant]=[]
            for station in antenne.stations :
                exp = station.exploitant
                tec = []
                for emetteur in station.emetteurs:
                    tec.append(emetteur.systeme)
                dico_ant.setdefault(ant,[]).append([exp,tec])
        return dico_ant
    def get_stations_id(self):
        return set([station.id for antenne in self.antennes for station in antenne.stations])
    def get_stations_comsis(self):
        return set([station.comsis for antenne in self.antennes for station in antenne.stations])
    def iter_sys_exp(self):
        dico = {}
        for antenne in self.antennes:
            for station in antenne.stations:
                exploitant = station.exploitant
                for emetteur in station.emetteurs:
                    dico.setdefault(emetteur.systeme,[]).append(exploitant)
        return dico
    def iter_pro_exp(self):
        dico = {}
        proprietaire = self.proprietaire
        for antenne in self.antennes:
            for station in antenne.stations:
                dico.setdefault(proprietaire,[]).append(station.exploitant)
        return dico

class Antenne(object):
    __slots__ = ('type', 'dimension', 'rayon', 'azimut', 'altitude','stations')
    def __init__(self, type, dimension, rayon, azimut, altitude):
        self.type = type
        self.dimension, self.rayon = dimension, rayon
        self.azimut, self.altitude = azimut, altitude
        self.stations = []
    def get_exploitant_gsm(self):
        liste = []
        l_gsm = ['GSM R', 'GSM 1800', 'GSM 1800/GSM 900', 'GSM 900', 'GSM 900/GSM 1800', 'GSM 900/UMTS 2100',
        'UMTS 2100', 'UMTS 2100/UMTS 900', 'UMTS 900', 'LTE 1800', 'LTE 2600', 'LTE 700', 'LTE 800']
        for station in self.stations:
            for emetteur in station.emetteurs:
                if emetteur.systeme in l_gsm:
                    liste.append(station.exploitant)
        return set(liste)
    def toDICT(self):
        dico = {}
        dico['type'] = self.type
        dico['dimension'] = self.dimension
        dico['rayon'] = self.rayon
        dico['azimut'] = self.azimut
        dico['altitude'] = self.altitude
        dico['stations'] = [station.toDICT() for station in self.stations]
        return dico

class Station(object):
    __slots__ = ('id', 'exploitant', 'comsis', 'dateImplan', 'dateModif', 'dateService','emetteurs')
    def __init__(self, id, exploitant, comsis, dateImplan, dateModif, dateService):
        self.id = id
        self.exploitant = exploitant
        self.comsis = comsis
        self.dateImplan = dateImplan
        self.dateModif = dateModif
        self.dateService = dateService
        self.emetteurs = []
    def toDICT(self):
        dico = {}
        dico['id'] = self.id
        dico['exploitant'] = self.exploitant
        dico['comsis'] = self.comsis
        dico['dateImplan'] = self.dateImplan
        dico['dateModif'] = self.dateModif
        dico['dateService'] = self.dateService
        dico['emetteurs'] = [emetteur.toDICT() for emetteur in self.emetteurs]
        return dico

class Emetteur():
    __slots__ = ('systeme', 'bandes')
    def __init__(self, systeme):
        self.systeme=systeme
        self.bandes = []
    def toDICT(self):
        dico = {}
        dico['systeme'] = self.systeme
        dico['bandes'] = [bande.toDICT() for bande in self.bandes]
        return dico
class Bande():
    __slots__ = ('debut', 'fin', 'unite')
    def __init__(self, debut, fin, unite):
        self.debut = debut
        self.fin = fin
        self.unite = unite
    def toDICT(self):
        dico = {}
        dico['debut'] = self.debut
        dico['fin'] = self.fin
        dico['unite'] = self.unite
        return dico
