# -*- coding: utf-8 -*-

dir_zip = "/zip/"
dir_csv = "/csv/"
dir_json = "/json/"
anfr_data = "https://www.data.gouv.fr/fr/datasets/donnees-sur-les-installations-radioelectriques-de-plus-de-5-watts-1/"
insee_link = "https://datanova.legroupe.laposte.fr/explore/dataset/laposte_hexasmal/download/?format=csv&timezone=Europe/Paris&use_labels_for_header=true"
insee_file = "/code-insee.csv"
img_01 = 'img/image01.gif'
img_02 = 'img/image02.gif'
