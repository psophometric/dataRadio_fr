#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from utils import *
from Tkinter import *
import ttk, tkFileDialog, tkMessageBox, tkFont
from itertools import chain
import webbrowser
import json
import os
import re

class App(Tk):
    def __init__(self, parent):
        self.parent = parent
        self.initUI()
        self.config={}
    def initUI(self):
        self.parent.title("dataRadio_fr")
        ### MENU
        menubar = Menu(self.parent)
        self.parent.config(menu=menubar)
        fileMenu = Menu(menubar)
        submenu = Menu(fileMenu)
        submenu.add_command(label="Import CSV", command=self.importCSV)
        submenu.add_command(label="Import JSON", command=self.importJSON)
        submenu.add_command(label="Import Internet", command=self.importInternet)
        fileMenu.add_cascade(label='Import', menu=submenu, underline=0)
        fileMenu.add_separator()
        fileMenu.add_command(label="Quitter", underline=0, command=self.onExit)
        menubar.add_cascade(label="Fichier", underline=0, menu=fileMenu)
        ### ONGLETS
        self.n = ttk.Notebook(self.parent)
        self.f1 = ttk.Frame(self.n)
        self.f2 = ttk.Frame(self.n)
        self.f3 = ttk.Frame(self.n)
        self.f4 = ttk.Frame(self.n)
        self.f5 = ttk.Frame(self.n)
        self.n.add(self.f1, text='GSM')
        self.n.add(self.f2, text='Autres')
        self.n.add(self.f3, text='ID')
        self.n.add(self.f4, text='MutuGSM')
        self.n.add(self.f5, text='inteRfer')
        self.n.grid(column=0, row=0, columnspan=1, sticky="nsew")
        self.f1_populate()
        self.f2_populate()
        self.f3_populate()
        self.f4_populate()
        self.f5_populate()

        # Frame1 : affichage supports (treeview)
        self.Frame1 = ttk.Frame(self.parent, borderwidth=2, relief='groove')
        self.Frame1.grid(column=0, row=1, columnspan = 2,sticky="nsew")
        # Frame2 : info support (labels droits)
        self.Frame2 = ttk.Frame(self.parent, borderwidth=2, relief='groove', width=300)
        self.Frame2.grid(column=3, row=0, rowspan=2, sticky="nsew")
        # Frame3 : Boutons post-extraction (export CSV, Map, ...)
        self.Frame3 = ttk.Frame(self.parent, borderwidth=2, relief='groove')
        self.Frame3.grid(column=0, row=2, columnspan = 2, sticky="nsew")

        # REDIMENSIONNEMENT FENETRE
        self.n.columnconfigure(0, weight=1)
        self.f1.columnconfigure(0, weight=1)
        self.f2.columnconfigure(0, weight=1)
        self.f3.columnconfigure(0, weight=1)
        self.f4.columnconfigure(0, weight=1)
        self.f5.columnconfigure(0, weight=1)
        self.Frame1.columnconfigure(0, weight=10)
        self.Frame1.rowconfigure(0, weight=10)
        self.parent.columnconfigure(0, weight=1)
        self.parent.rowconfigure(1, weight=10)

        # TREEVIEW SUPPORTS
        self.tree = ttk.Treeview(self.Frame1, selectmode='browse')
        self.vsb = ttk.Scrollbar(self.Frame1, orient="vertical", command=self.tree.yview)
        self.tree.configure(yscrollcommand=self.vsb.set)
        self.tree["columns"]=("one","two")
        self.tree.column("one", minwidth=100, width=250, stretch=True)
        self.tree.column("two", minwidth=100, width=250, stretch=True)
        self.tree.heading("one", text="")
        self.tree.heading("two", text="")
        self.tree.bind('<<TreeviewSelect>>', self.selectItem)
        self.vsb.grid(sticky='nsew', column = 1, row = 0, ipady = 100)
        self.tree.grid(sticky='NSEW', column=0, row=0)
        self.selectItemInit()

    def f1_populate(self):
        self.Frame10 = ttk.LabelFrame(self.f1, text='systèmes')
        self.Frame10.grid(column=0, row=0, columnspan=7, rowspan=2,  sticky="ew")
        self.Frame11 = ttk.LabelFrame(self.f1, text='exploitants')
        self.Frame11.grid(column=0, row=2,columnspan=7, rowspan=2, sticky="ew")
        self.Frame12 = ttk.LabelFrame(self.f1, text='département')
        self.Frame12.grid(column=0, row=4,columnspan=7, rowspan=2, sticky="ew")
        self.Frame14 = ttk.Frame(self.f1, borderwidth=0, relief='groove')
        self.Frame14.grid(column=8, row=0, rowspan=6, sticky='ew')
        systeme = ['GSM R', 'GSM 1800', 'GSM 1800/GSM 900', 'GSM 900', 'GSM 900/GSM 1800', 'GSM 900/UMTS 2100',
        'UMTS 2100', 'UMTS 2100/UMTS 900', 'UMTS 900', 'LTE 1800', 'LTE 2600', 'LTE 700', 'LTE 800']
        self.systeme_checked = []
        for ix, text in enumerate(systeme):
            self.var10=StringVar()
            self.systeme_checked.append(Checkbutton(self.Frame10, text=text,
                                        onvalue=text, offvalue = '',
                                        variable=self.var10))
            if ix < 7:
                self.systeme_checked[ix].grid(row=1, column=ix, sticky='w')
            else :
                self.systeme_checked[ix].grid(row=2, column=(ix-7), sticky='w')
            self.systeme_checked[ix].deselect()
            self.systeme_checked[-1].variable=self.var10


        exploitant = ['ORANGE', 'SFR', "BOUYGUES TELECOM", "FREE MOBILE", "SNCF Réseau"]
        self.exploitant_checked = []
        for ix, text in enumerate(exploitant):
            self.var20=StringVar()
            self.exploitant_checked.append(Checkbutton(self.Frame11, text=text,
                                        onvalue=text, offvalue = '',
                                        variable=self.var20))
            self.exploitant_checked[ix].grid(row=1, column=ix, sticky='w')
            self.exploitant_checked[ix].deselect()
            self.exploitant_checked[-1].variable=self.var20

        #departement
        self.departement1_select = StringVar()
        self.departement1_choix = list(limitrophes.limit.keys())
        self.departement1_liste = ttk.Combobox(self.Frame12, textvariable = self.departement1_select, \
						values = self.departement1_choix, state = 'readonly')
        self.departement1_liste.current(0)
        self.departement1_liste.grid(row=1, column=1)

        self.button_tri = Button(self.Frame14, text="tri", command=self.tri_GSM)
        self.button_tri.grid(row=0,column=0, sticky='nsew')
    def f2_populate(self):
        self.Frame20 = ttk.LabelFrame(self.f2, text='système')
        self.Frame20.grid(column=0, row=2, columnspan=7, rowspan=2, sticky="ew")
        self.sys_entry1 = Entry(self.Frame20)
        self.sys_entry1.grid(row=0, column=0)
        self.sys_entry2 = Entry(self.Frame20)
        self.sys_entry2.grid(row=0, column=1)
        self.sys_entry3 = Entry(self.Frame20)
        self.sys_entry3.grid(row=0, column=2)

        self.Frame21 = ttk.LabelFrame(self.f2, text='exploitant')
        self.Frame21.grid(column=0, row=4, columnspan=7, rowspan=2, sticky="ew")
        self.exp_entry1 = Entry(self.Frame21)
        self.exp_entry1.grid(row=0, column=0)
        self.exp_entry2 = Entry(self.Frame21)
        self.exp_entry2.grid(row=0, column=1)
        self.exp_entry3 = Entry(self.Frame21)
        self.exp_entry3.grid(row=0, column=2)

        self.Frame22 = ttk.LabelFrame(self.f2, text='département')
        self.Frame22.grid(column=0, row=6, columnspan=7, rowspan=2, sticky="ew")
        self.departement2_select = StringVar()
        self.departement2_choix = list(limitrophes.limit.keys())
        self.departement2_liste = ttk.Combobox(self.Frame22, textvariable = self.departement2_select, \
						values = self.departement2_choix, state = 'readonly')
        self.departement2_liste.current(0)
        self.departement2_liste.grid(row=0, column=1)

        self.Frame24 = ttk.Frame(self.f2, borderwidth=0, relief='groove')
        self.Frame24.grid(column=8, row=0, rowspan=6, sticky='ew')
        self.button_tri = Button(self.Frame24, text="tri", command=self.tri_AUTRE)
        self.button_tri.grid(row=0,column=0, sticky='nsew')

    def f3_populate(self):
        self.Frame30 = ttk.LabelFrame(self.f3, text='Recherche par ID')
        self.Frame30.grid(column=0, row=0, columnspan=7, rowspan=2, sticky="ew")
        self.Frame31 = ttk.LabelFrame(self.f3, text='Itération des supports')
        self.Frame31.grid(column=0, row=2, columnspan=7, rowspan=2, sticky="ew")

        self.id_select = StringVar()
        self.id_choix = ['Support','Station','COMSIS']
        self.id_liste = ttk.Combobox(self.Frame30, textvariable = self.id_select,
						values = self.id_choix, state = 'readonly')
        self.id_liste.current(0)
        self.id_liste.grid(row=0, column=0)
        self.id_entry = Entry(self.Frame30)
        self.id_entry.grid(row=0, column=1)
        self.button_id = Button(self.Frame30, text="Rechercher", command=self.recherche_id)
        self.button_id.grid(row=0,column=3, sticky='nsew')

        self.button_id = Button(self.Frame31, text="Itération Systèmes/Exploitants", command=self.iteration_sys_exp)
        self.button_id.grid(row=0,column=0, sticky='nsew')
        self.button_id = Button(self.Frame31, text="Itération Propriétaires/Exploitants", command=self.iteration_pro_exp)
        self.button_id.grid(row=0,column=1, sticky='nsew')

    def f4_populate(self):
        self.Frame40 = ttk.LabelFrame(self.f4, text='Type de mutualisation')
        self.Frame40.grid(column=0, row=0, columnspan=7, rowspan=2, sticky="ew")
        self.Frame41 = ttk.LabelFrame(self.f4, text='Opérateurs')
        self.Frame41.grid(column=0, row=2, columnspan=7, rowspan=2, sticky="ew")
        # Frame44 : tri mutu
        self.Frame44 = ttk.Frame(self.f4, borderwidth=0, relief='groove')
        self.Frame44.grid(column=8, row=0, rowspan=6, sticky="EW")

        self.mutu_select = StringVar()
        self.mutu_choix = ['Support','Antenne']
        self.mutu_liste = ttk.Combobox(self.Frame40, textvariable = self.mutu_select,
						values = self.mutu_choix, state = 'readonly')
        self.mutu_liste.current(0)
        self.mutu_liste.grid(row=0, column=0)

        label = Label(self.Frame41, text="ORANGE").grid(row=0, column=0)
        label = Label(self.Frame41, text="SFR").grid(row=0, column=1)
        label = Label(self.Frame41, text="BOUYGUES").grid(row=0, column=2)
        label = Label(self.Frame41, text="FREE").grid(row=0, column=3)
        label = Label(self.Frame41, text="Gsm-R").grid(row=0, column=4)
        self.mutu_choix = ['Présent','Absent','Indifférent']
        self.orange_select = StringVar()
        self.sfr_select = StringVar()
        self.bouygues_select = StringVar()
        self.free_select = StringVar()
        self.gsmr_select = StringVar()

        self.mutu_orange = ttk.Combobox(self.Frame41, textvariable = self.orange_select,
						values = self.mutu_choix, state = 'readonly')
        self.mutu_orange.current(0)
        self.mutu_orange.grid(row=1, column=0)
        self.mutu_sfr = ttk.Combobox(self.Frame41, textvariable = self.sfr_select,
						values = self.mutu_choix, state = 'readonly')
        self.mutu_sfr.current(2)
        self.mutu_sfr.grid(row=1, column=1)
        self.mutu_bouygues = ttk.Combobox(self.Frame41, textvariable = self.bouygues_select,
						values = self.mutu_choix, state = 'readonly')
        self.mutu_bouygues.current(2)
        self.mutu_bouygues.grid(row=1, column=2)
        self.mutu_free = ttk.Combobox(self.Frame41, textvariable = self.free_select,
						values = self.mutu_choix, state = 'readonly')
        self.mutu_free.current(2)
        self.mutu_free.grid(row=1, column=3)
        self.mutu_gsmr = ttk.Combobox(self.Frame41, textvariable = self.gsmr_select,
						values = self.mutu_choix, state = 'readonly')
        self.mutu_gsmr.current(2)
        self.mutu_gsmr.grid(row=1, column=4)

        self.button_tri = Button(self.Frame44, text="tri", command=self.mutugsm)
        self.button_tri.grid(row=0,column=0)

    def f5_populate(self):
        self.Frame50 = ttk.LabelFrame(self.f5, text='systèmes interférents')
        self.Frame50.grid(column=0, row=0, columnspan=7, rowspan=2, sticky="ew")
        self.Frame51 = ttk.LabelFrame(self.f5, text='exploitants interférents')
        self.Frame51.grid(column=0, row=2, columnspan=7, rowspan=2, sticky="ew")
        self.Frame52 = ttk.LabelFrame(self.f5, text='azimuts')
        self.Frame52.grid(column=0, row=4, columnspan=7, rowspan=2, sticky="ew")
        self.Frame53 = ttk.LabelFrame(self.f5, text='département')
        self.Frame53.grid(column=6, row=2, columnspan=2, rowspan=2)
        # Frame54 : tri inteRfer
        self.Frame54 = ttk.Frame(self.f5, borderwidth=0, relief='groove')
        self.Frame54.grid(column=8, row=0, rowspan=6, sticky="EW")
        systeme = ['2G (900 Mhz)', '3G (900 Mhz)']
        self.systeme_interfer_checked = []
        for ix, text in enumerate(systeme):
            self.var50=StringVar()
            self.systeme_interfer_checked.append(Checkbutton(self.Frame50, text=text,
                                        onvalue=text, offvalue = '',
                                        variable=self.var50))
            self.systeme_interfer_checked[ix].grid(row=1, column=ix, sticky='w')
            self.systeme_interfer_checked[ix].deselect()
            self.systeme_interfer_checked[-1].variable=self.var50


        exploitant = ['ORANGE', 'SFR', "BOUYGUES TELECOM", "FREE MOBILE"]
        self.exploitant_interfer_checked = []
        for ix, text in enumerate(exploitant):
            self.var51=StringVar()
            self.exploitant_interfer_checked.append(Checkbutton(self.Frame51, text=text,
                                        onvalue=text, offvalue = '',
                                        variable=self.var51))
            self.exploitant_interfer_checked[ix].grid(row=1, column=ix, sticky='w')
            self.exploitant_interfer_checked[ix].deselect()
            self.exploitant_interfer_checked[-1].variable=self.var51
        #AZIMUT
        self.var5=StringVar()
        self.option_azimut = Checkbutton(self.Frame52, text='Azimuts',
                                    onvalue=1, offvalue = 0,
                                    variable=self.var5)
        self.option_azimut.grid(row=0, column=0, sticky='w')
        self.option_azimut.deselect()
        label = Label(self.Frame52, text="Gsm-R :").grid(row=1, column=1)
        label = Label(self.Frame52, text="Dist. min. (km)").grid(row=0, column=2)
        self.distanceGm = Entry(self.Frame52)
        self.distanceGm.grid(row=1, column=2)
        label = Label(self.Frame52, text="Dist. max. (km)").grid(row=0, column=3)
        self.distanceGM = Entry(self.Frame52)
        self.distanceGM.grid(row=1, column=3)
        self.distanceGM.insert(END, '3')
        label = Label(self.Frame52, text="Faisceau (°)").grid(row=0, column=4)
        self.faisceau = Entry(self.Frame52)
        self.faisceau.grid(row=1, column=4)
        label = Label(self.Frame52, text="Gsm Publics :").grid(row=1, column=5)
        label = Label(self.Frame52, text="Dist. max. (km)").grid(row=0, column=6)
        self.distanceP = Entry(self.Frame52)
        self.distanceP.insert(END, '1')
        self.distanceP.grid(row=1, column=6)

        #departement
        self.departement_interfer_select = StringVar()
        self.departement_interfer_choix = list(limitrophes.limit.keys())
        self.departement_interfer_liste = ttk.Combobox(self.Frame53, textvariable = self.departement_interfer_select, \
						values = self.departement_interfer_choix, state = 'readonly')
        self.departement_interfer_liste.current(0)
        self.departement_interfer_liste.grid(row=1, column=1)

        self.button_tri = Button(self.Frame54, text="tri", command=self.interfer)
        self.button_tri.grid(row=0,column=0)

    def selectItemInit(self):
        self.label0 = ttk.Label(self.Frame2, text='______________________Infos Support______________________')
        self.label0.grid(row=0, column=0, columnspan=2)

    def selectItem(self, a):
        item = self.tree.selection()[0]
        try:
            for widget in self.Frame2.winfo_children():
                widget.destroy()
        except:
            pass
        try:
            datasup = dictionnaire[self.tree.item(item,"tags")[0]]
        except:
            self.selectItemInit()
        else:
            self.selectItemInit()
            k = 3
            for att in datasup.__slots__:
                i = att
                j= getattr(datasup,att)
                self.label1 = ttk.Label(self.Frame2, text=i)
                self.label1.grid(row=k, column=0)
                if i == 'antennes': j = len(j)
                self.label2 = ttk.Label(self.Frame2, text=j)
                self.label2.grid(row=k, column=1)
                k+=1

            self.lat , self.lon = datasup.lat, datasup.lon
            self.id = datasup.id

            self.button_cartoradio = Button(self.Frame2, text="Voir sur cartoradio", command=self.cartoradio).grid(row=k,column=0)
            self.button_json = Button(self.Frame2, text="Voir le JSON", command=self.json).grid(row=k,column=1)

    def recherche_id(self):
        def sup_id():
            try :
                self.l_result.append(opt_id)
                self.affiche_support(opt_id, 1)
                try:
                    for widget in self.Frame3.winfo_children():
                        widget.destroy()
                except:
                    pass
            except :
                self.top.destroy()
                self.after_tri(0)
                tkMessageBox.showinfo("Erreur", "Support introuvable.\nVérifiez ID.")
        def sta_id():
            i=0
            for support in dictionnaire.keys():
                list_sta_id = dictionnaire[support].get_stations_id()
                if opt_id in list_sta_id:
                    i+=1
                    self.l_result.append(support)
                    self.affiche_support(support, i)
                    self.after_tri(i)
            if i==0 :
                self.top.destroy()
                try:
                    for widget in self.Frame3.winfo_children():
                        widget.destroy()
                except:
                    pass
                tkMessageBox.showinfo("Erreur", "Station introuvable.\nVérifiez ID.")
        def comsis_id():
            i=0
            for support in dictionnaire.keys():
                list_sta_comsis = dictionnaire[support].get_stations_comsis()
                if opt_id in list_sta_comsis:
                    i+=1
                    self.l_result.append(support)
                    self.affiche_support(support, i)
                    self.after_tri(i)

            if i==0 :
                self.top.destroy()
                try:
                    for widget in self.Frame3.winfo_children():
                        widget.destroy()
                except:
                    pass
                tkMessageBox.showinfo("Erreur", "Station introuvable.\nVérifiez COMSIS.")

        opt_type = self.id_liste.get()
        opt_id=self.id_entry.get()
        if not dictionnaire:
            tkMessageBox.showinfo("Absence de données", "Veuillez importer des données (Fichier/Import)")
        elif opt_id == '':
            tkMessageBox.showinfo("Id Vide", "Veuillez indiquer un 'ID'")
        else:
            self.tree.delete(*self.tree.get_children()) # REMISE A ZERO DES SUPPORTS AFFICHES
            self.l_result=[]
            self.top = Top(self.parent,constantes.img_02)
            self.top.update()
            if opt_type == 'Support':
                sup_id()
            elif opt_type == 'Station':
                sta_id()
            else:
                comsis_id()
            self.top.destroy()

    def iteration_sys_exp(self):
        def chargement():
            self.tree.delete(*self.tree.get_children()) # REMISE A ZERO DES SUPPORTS AFFICHES
            d_result={}
            ### RECHERCHE
            for support in dictionnaire.keys():
                dico = dictionnaire[support].iter_sys_exp()
                for key, values in dico.items():
                    d_result.setdefault(key,[]).extend(values)
            ### AFFICHAGE
            i=0
            #print "Systeme ; Exploitant ; Nombre"
            for systeme in d_result.keys():
                if systeme != '':
                    exploitants = d_result[systeme]
                    self.tree.insert("", i, systeme, text=systeme, values=(len(d_result[systeme]), len(set(d_result[systeme]))), tags=systeme)
                    for exploitant in set(exploitants):
                        self.tree.insert(systeme, i, text=exploitant,values=(exploitants.count(exploitant), ''), tags=systeme)
                        #print "%s ; %s ; %s" % (systeme, exploitant, exploitants.count(exploitant))
                        i+=1
            try:
                for widget in self.Frame3.winfo_children():
                    widget.destroy()
            except:
                pass
        if not dictionnaire:
            tkMessageBox.showinfo("Absence de données", "Veuillez importer des données (Fichier/Import)")
        else:
            self.top = Top(self.parent,constantes.img_02)
            self.top.update()
            chargement()
            self.top.destroy()

    def iteration_pro_exp(self):
        def chargement():
            self.tree.delete(*self.tree.get_children()) # REMISE A ZERO DES SUPPORTS AFFICHES
            d_result={}
            ### RECHERCHE
            for support in dictionnaire.keys():
                dico = dictionnaire[support].iter_pro_exp()
                for key, values in dico.items():
                    d_result.setdefault(key,[]).extend(values)
            ### AFFICHAGE
            i=0
            for proprio in d_result.keys():
                if proprio != '':
                    exploitants = d_result[proprio]
                    self.tree.insert("", i, proprio, text=proprio, values=(len(d_result[proprio]), len(set(d_result[proprio]))), tags=proprio)
                    for exploitant in set(exploitants):
                        self.tree.insert(proprio, i, text=exploitant,values=(exploitants.count(exploitant), ''), tags=proprio)
                        #print "%s ; %s ; %s" % (proprio, exploitant, exploitants.count(exploitant))
                        i+=1
            try:
                for widget in self.Frame3.winfo_children():
                    widget.destroy()
            except:
                pass
        if not dictionnaire:
            tkMessageBox.showinfo("Absence de données", "Veuillez importer des données (Fichier/Import)")
        else:
            self.top = Top(self.parent,constantes.img_02)
            self.top.update()
            chargement()
            self.top.destroy()

    def mutugsm(self):
        def chargement_support():
            self.tree.delete(*self.tree.get_children()) # REMISE A ZERO DES SUPPORTS AFFICHES
            self.l_result=[]
            i = 0
            for support in dictionnaire.keys():
                mutu = False
                result = dictionnaire[support].get_exploitant_gsm()
                if len(result) > 1:
                    for exp in result:
                        if exp in set(l_absent):
                            mutu = None
                            break
                    if mutu != None:
                        test = (result & set(l_present))
                        if len(test) == len(l_present):
                            mutu = True
                if mutu:
                    i+=1
                    self.affiche_support(support,i)
                    self.l_result.append(support)
            self.after_tri(i)
        def chargement_antenne():
            self.tree.delete(*self.tree.get_children()) # REMISE A ZERO DES SUPPORTS AFFICHES
            self.l_result=[]
            i = 0
            for support in dictionnaire.keys():
                mutu = False
                for antenne in dictionnaire[support].antennes:
                    result = antenne.get_exploitant_gsm()
                    if len(result) > 1:
                        for exp in result:
                            if exp in set(l_absent):
                                mutu = None
                                break
                        if mutu != None:
                            test = result & set(l_present)
                            if len(test) == len(l_present):
                                mutu = True
                if mutu:
                    i+=1
                    self.affiche_support(support,i)
                    self.l_result.append(support)
            self.after_tri(i)

        mutu = self.mutu_select.get()
        d={}
        d['ORANGE'] = self.orange_select.get()
        d['SFR'] = self.sfr_select.get()
        d['BOUYGUES TELECOM'] = self.bouygues_select.get()
        d['FREE MOBILE'] = self.free_select.get()
        d['SNCF Réseau'] = self.gsmr_select.get()
        l_present, l_absent, l_indif = [], [], []
        for ope, status in d.items():
            if status == 'Présent':
                l_present.append(ope)
            elif status == "Absent":
                l_absent.append(ope)
            else:
                l_indif.append(ope)
        if not dictionnaire:
            tkMessageBox.showinfo("Absence de données", "Veuillez importer des données (Fichier/import)")
        else :
            self.top = Top(self.parent,constantes.img_02)
            self.top.update()
            if mutu == "Antenne" : chargement_antenne()
            if mutu == "Support" : chargement_support()
            self.top.destroy()

    def interfer(self):
        def chargement():
            self.tree.delete(*self.tree.get_children()) # REMISE A ZERO DES SUPPORTS AFFICHES
            GsmP = liste_systemes_interfer
            GsmR = ['GSM R']

            if self.faisceau.get() != '':
                opt_faisceau = float(self.faisceau.get().replace(',','.'))
            else :
                opt_faisceau = False
            if self.var5.get() == '1':
                opt_azimut = True
            else:
                opt_azimut = False
            distanceGM = float(self.distanceGM.get().replace(',','.'))
            distanceP = float(self.distanceP.get().replace(',','.'))
            if self.distanceGm.get() != '':
                distanceGm = float(self.distanceGm.get().replace(',','.'))
            else:
                distanceGm = False
            if opt_departement:
                opt_departement_limitrof = limitrophes.limit[opt_departement]
                opt_departement_limitrof.append(opt_departement)
            else:
                opt_departement_limitrof = False
            l_support_GSMP = [support for support in dictionnaire.keys() if dictionnaire[support].tri(opt_departement_limitrof,opt_exploitants, GsmP)]
            l_support_GSMR = [support for support in dictionnaire.keys() if dictionnaire[support].tri(opt_departement,['SNCF Réseau'], GsmR)]
            d_voisin, d_azimut = interfer.liste_voisin(l_support_GSMR,distanceGM, distanceGm, distanceP, dictionnaire,l_support_GSMP, GsmR, GsmP, opt_exploitants, opt_azimut, opt_faisceau)

            i=0
            if d_voisin:
                for supportG, supportsP in d_voisin.items():
                    o_sup = dictionnaire[supportG]
                    i+=1
                    data_sup = o_sup.show_support()
                    self.tree.insert("", i, supportG, text=supportG, values=(data_sup[0], data_sup[1]), tags=supportG)
                    for supportP in supportsP:
                        o_sup = dictionnaire[supportP]
                        i+=1
                        data_sup = o_sup.show_support()
                        self.tree.insert(supportG, i, text=supportP, values=(data_sup[0], data_sup[1]), tags=supportP)
                self.l_result_R = [support for support in d_voisin.keys()]
                self.l_result_P = [supportP for supportG, supportsP in d_voisin.items() for supportP in supportsP]
                self.l_azimut_R = [azimut for azimut in d_azimut['gsmR']]
                self.l_azimut_P = [azimut for azimut in d_azimut['gsmP']]
            else:
                self.l_result_R, self.l_result_P = [], []
                self.l_azimut_R, self.l_azimut_P = [], []
            self.after_tri_interfer(i)

        opt_systemes_interfer=set([chkbox.variable.get() for chkbox in self.systeme_interfer_checked])
        opt_exploitants_interfer=set([chkbox.variable.get() for chkbox in self.exploitant_interfer_checked])
        opt_departement = self.departement_interfer_liste.get()
        opt_systemes_interfer = [value for value in opt_systemes_interfer if value != '']
        opt_exploitants = [value for value in opt_exploitants_interfer if value != '']
        liste_systemes_interfer=[]
        liste_2G_900 = ['GSM 1800/GSM 900', 'GSM 900', 'GSM 900/GSM 1800', 'GSM 900/UMTS 2100']
        liste_3G_900 = ['UMTS 2100/UMTS 900', 'UMTS 900']
        if '2G (900 Mhz)' in opt_systemes_interfer : liste_systemes_interfer.extend(liste_2G_900)
        if '3G (900 Mhz)' in opt_systemes_interfer : liste_systemes_interfer.extend(liste_3G_900)
        regex = True
        r = re.compile(r"^\d*[.,]?\d*$")
        if not r.match(self.distanceGM.get()):regex=False
        if not r.match(self.distanceGm.get()):regex=False
        if not r.match(self.faisceau.get()):regex = False
        if not r.match(self.distanceP.get()):regex = False
        if not interfer.geopy_info:
            tkMessageBox.showinfo("Absence de module", "Erreur d'importation du module geopy\nPour installer le module :\npip install geopy")
        elif not dictionnaire:
            tkMessageBox.showinfo("Absence de données", "Veuillez importer des données (Fichier/Import)")
        elif len(liste_systemes_interfer) == 0:
            tkMessageBox.showinfo("Liste Vide", "Veuillez selectionner au moins un système")
        elif len(opt_exploitants) == 0:
            tkMessageBox.showinfo("Liste Vide", "Veuillez selectionner au moins un exploitant")
        elif not regex:
            tkMessageBox.showinfo("Erreur Saisie", "Erreur de saisie champs azimuts.\nVeuillez saisir uniquement des chiffres sans unité")
        else :
            if opt_departement == '': opt_departement=False #aucun departement selectionné
            self.top = Top(self.parent,constantes.img_01)
            self.top.update()
            chargement()
            self.top.destroy()

    def affiche_support(self, support, i):
        o_sup = dictionnaire[support]
        data_sup = o_sup.show_support()
        self.tree.insert("", i, support, text=support, values=(data_sup[0], data_sup[1]), tags=support)
        data_ant = o_sup.show_antennes()
        for ant in data_ant.keys():
            for eme in data_ant[ant]:
                self.tree.insert(support, i, text=ant,values=(eme[0],eme[1]), tags=support)
        return support

    def after_tri(self, i):
        try:
            for widget in self.Frame3.winfo_children():
                widget.destroy()
        except:
            pass
        self.button_export = Button(self.Frame3, text="Export Supports JSON", command=self.export_json).grid(row=0,column=0)
        self.button_json = Button(self.Frame3, text="Export Supports MAP", command=self.export_map).grid(row=0,column=1)
        text = "Nombre de supports affichés : %s" % (i)
        label_nb = Label(self.Frame3, text=text).grid(row=0, column=2)
    def after_tri_interfer(self, i):
        try:
            for widget in self.Frame3.winfo_children():
                widget.destroy()
        except:
            pass
        self.button_export = Button(self.Frame3, text="Export Supports JSON", command=self.export_json_interfer).grid(row=0,column=0)
        self.button_json = Button(self.Frame3, text="Export Supports MAP", command=self.export_map_interfer).grid(row=0,column=1)
        text = "Nombre de supports affichés : %s" % (i)
        label_nb = Label(self.Frame3, text=text).grid(row=0, column=2)

    def export_json(self):
        def chargement(filename):
            dico_json = {}
            dico_json['dataset'] = 'Les installations radioélectriques de plus de 5 watts'
            dico_json['producteur'] = 'Anfr'
            dico_json['date'] = self.config['date']
            dico_json['supports'] = [dictionnaire[support].toDICT() for support in self.l_result]
            with open(filename, 'w') as f:
                j = json.dumps(dico_json, indent=4)
                f.write(j)

        repertoire = dir_path + constantes.dir_json
        filename = tkFileDialog.asksaveasfilename(initialdir = repertoire,title = "Select file",
                                                    filetypes = (("json files","*.json"),("all files","*.*")),
                                                    defaultextension='.json')
        self.top = Top(self.parent,constantes.img_02)
        self.top.update()
        if filename :
            chargement(filename)
        self.top.destroy()

    def export_json_interfer(self):
        def chargement(filename):
            dico_json = {}
            self.l_result = chain(self.l_result_P,self.l_result_R)
            dico_json['dataset'] = 'Les installations radioélectriques de plus de 5 watts'
            dico_json['producteur'] = 'Anfr'
            dico_json['date'] = self.config['date']
            dico_json['supports'] = [dictionnaire[support].toDICT() for support in self.l_result]
            with open(filename, 'w') as f:
                j = json.dumps(dico_json, indent=4)
                f.write(j)

        repertoire = dir_path + constantes.dir_json
        filename = tkFileDialog.asksaveasfilename(initialdir = repertoire,title = "Select file",
                                                    filetypes = (("json files","*.json"),("all files","*.*")),
                                                    defaultextension='.json')
        self.top = Top(self.parent,constantes.img_01)
        self.top.update()
        if filename :
            chargement(filename)
        self.top.destroy()

    def export_map(self):
        def chargement(filename):
            map_osm = map.creation_map()
            map.affichage_support(map_osm,self.l_result,'red',False,dictionnaire)
            map.sauvegarde_map(map_osm,filename, True)

        if not map.folium_info:
            tkMessageBox.showinfo("Absence de module", "Erreur d'importation du module folium\nPour installer le module :\npip install folium")
        else:
            repertoire = dir_path
            filename = tkFileDialog.asksaveasfilename(initialdir = repertoire,title = "Select file",
                                                        filetypes = (("html files","*.html"),("all files","*.*")),
                                                        defaultextension='.html')
            if filename :
                self.top = Top(self.parent,constantes.img_02)
                self.top.update()
                chargement(filename)
                self.top.destroy()

    def export_map_interfer(self):
        def chargement(filename):
            map_osm = map.creation_map()
            map.affichage_support(map_osm,self.l_result_P,'red',False,dictionnaire)
            map.affichage_support(map_osm,self.l_result_R,'green',False,dictionnaire)
            map.affichage_azimut(map_osm, self.l_azimut_P, 'red')
            map.affichage_azimut(map_osm, self.l_azimut_R, 'green')
            map.sauvegarde_map(map_osm,filename, True)

        if not map.folium_info:
            tkMessageBox.showinfo("Absence de module", "Erreur d'importation du module folium\nPour installer le module :\npip install folium")
        else:
            repertoire = dir_path
            filename = tkFileDialog.asksaveasfilename(initialdir = repertoire,title = "Select file",
                                                        filetypes = (("html files","*.html"),("all files","*.*")),
                                                        defaultextension='.html')
            if filename :
                self.top = Top(self.parent,constantes.img_01)
                self.top.update()
                chargement(filename)
                self.top.destroy()

    def tri_GSM(self):
        def chargement():
            self.tree.delete(*self.tree.get_children()) # REMISE A ZERO DES SUPPORTS AFFICHES
            self.l_result = [self.affiche_support(support,0) for support in dictionnaire.keys() if dictionnaire[support].tri(opt_departement,opt_exploitants, opt_systemes)]
            self.after_tri(len(self.l_result))

        opt_systemes=set([chkbox.variable.get() for chkbox in self.systeme_checked])
        opt_exploitants=set([chkbox.variable.get() for chkbox in self.exploitant_checked])
        opt_departement = self.departement1_liste.get()
        opt_systemes = [value for value in opt_systemes if value != '']
        opt_exploitants = [value for value in opt_exploitants if value != '']
        if not dictionnaire:
            tkMessageBox.showinfo("Absence de données", "Veuillez importer des données (Fichier/import)")
        elif len(opt_systemes) == 0:
            tkMessageBox.showinfo("Liste Vide", "Veuillez selectionner au moins un système")
        elif len(opt_exploitants) == 0:
            tkMessageBox.showinfo("Liste Vide", "Veuillez selectionner au moins un exploitant")
        else :
            if opt_departement == '': opt_departement=False #aucun departement selectionné
            self.top = Top(self.parent,constantes.img_02)
            self.top.update()
            chargement()
            self.top.destroy()

    def tri_AUTRE(self):
        def chargement():
            self.tree.delete(*self.tree.get_children()) # REMISE A ZERO DES SUPPORTS AFFICHES
            self.l_result = [self.affiche_support(support,0) for support in dictionnaire.keys() if dictionnaire[support].tri(opt_departement,opt_exploitants, opt_systemes)]
            self.after_tri(len(self.l_result))

        opt_systemes=[self.sys_entry1.get(),self.sys_entry2.get(),self.sys_entry3.get()]
        opt_exploitants=[self.exp_entry1.get(),self.exp_entry2.get(),self.exp_entry3.get()]
        opt_departement = self.departement2_liste.get()

        opt_systemes = [value for value in opt_systemes if value != '']
        opt_exploitants = [value for value in opt_exploitants if value != '']

        if opt_departement == '': opt_departement=False #aucun departement selectionné
        if len(opt_systemes) == 0: opt_systemes=False
        if len(opt_exploitants) == 0: opt_exploitants=False

        if not dictionnaire:
            tkMessageBox.showinfo("Absence de données", "Veuillez importer des données (Fichier/import)")
        else :
            self.top = Top(self.parent,constantes.img_02)
            self.top.update()
            chargement()
            self.top.destroy()

    def cartoradio(self):
        #https://data.anfr.fr/map/?location=2,xx.xxxxx,y.yyyyy #location='zoom','lat','lon'
        #https://www.cartoradio.fr/cartoradio/web/#bbox/y.yyyy/xx.xxxx/y.yyyy/xx.xxxx/
        webbrowser.open('https://www.cartoradio.fr/cartoradio/web/#bbox/{0}/{1}/{0}/{1}/'.format(self.lon, self.lat))

    def json(self):
        data_json = dictionnaire[self.id].toDICT()
        djson = json.dumps(data_json, indent=4)
        top = Top_JSON(self.parent, djson)

    def onExit(self):
        self.parent.quit()

    def importCSV(self):
        def chargement(dirname):
            self.tree.delete(*self.tree.get_children()) # REMISE A ZERO DES SUPPORTS AFFICHES
            for widget in self.Frame3.winfo_children():
                widget.destroy()
            try:
                with open(dirname+'/config.json', 'r') as f:
                    self.config = json.load(f)
            except:
                self.config = {'DATA': '00000000', 'Tables_de_reference': '00000000'}
            self.config['date']='{0}-{1}-{2}'.format(self.config['DATA'][0:4],self.config['DATA'][4:6],self.config['DATA'][6:8])
            global dictionnaire
            dictionnaire = fichiers_csv.csv2dict_objets(dirname)
            if not dictionnaire:
                self.top.destroy()
                tkMessageBox.showinfo("Erreur", "Erreur lors du chargement des fichiers.\nVeuillez recharger la base (Fichier/Import/Import Internet)")

        repertoire = dir_path + constantes.dir_csv
        dirname = tkFileDialog.askdirectory(initialdir=repertoire,title='Please select a directory')
        if dirname:
            self.top = Top(self.parent,constantes.img_02)
            self.top.update()
            chargement(dirname)
            self.top.destroy()

    def importJSON(self):
        def chargement(filename):
            self.tree.delete(*self.tree.get_children()) # REMISE A ZERO DES SUPPORTS AFFICHES
            for widget in self.Frame3.winfo_children():
                widget.destroy()
            global dictionnaire
            dictionnaire, date = fichiers_json.json2dict_objets(filename)
            self.config['date'] = date
            if not dictionnaire:
                self.top.destroy()
                tkMessageBox.showinfo("Erreur", "Erreur lors du chargement du fichier JSON.")

        repertoire = dir_path + constantes.dir_json
        filename = tkFileDialog.askopenfilename(initialdir = repertoire,title = "Select file",filetypes = (("json files","*.json"),("all files","*.*")))
        if filename:
            self.top = Top(self.parent,constantes.img_02)
            self.top.update()
            chargement(filename)
            self.top.destroy()

    def importInternet(self):
        def chargement(dirname,repertoire_zip):
            datalink, datazip, reflink, refzip = fichiers_internet.verifier_site(constantes.anfr_data)
            try:
                with open(dirname+'/config.json', 'r') as f:
                    self.config = json.load(f)
            except:
                self.config = {'DATA': '0', 'Tables_de_reference': '0'}

            datalocal = self.config['DATA']
            reflocal = self.config['Tables_de_reference']
            maj = False
            if datazip > datalocal:
                fichiers_internet.telechargement(repertoire_zip, datalink, datazip)
                fichiers_internet.unzip(datazip, repertoire_zip, dirname)
                fichiers_internet.supprimer_repertoire(repertoire_zip)
                self.config['DATA']=datazip
                maj = True
            if refzip > reflocal:
                fichiers_internet.telechargement(repertoire_zip, reflink, refzip)
                fichiers_internet.unzip(refzip, repertoire_zip, dirname)
                fichiers_internet.supprimer_repertoire(repertoire_zip)
                self.config['Tables_de_reference']=refzip
                maj = True
            fichiers_internet.insee(dirname, constantes.insee_file, constantes.insee_link)

            self.top.destroy()
            if maj:
                with open(dirname+'/config.json', 'w') as f:
                    json.dump(self.config, f)
                tkMessageBox.showinfo("Maj OK", "Les données ont été mises à jour\nPour les utiliser allez dans le menu Fichier/Import/Import CSV.")
            else :
                tkMessageBox.showinfo("Déjà à jour", "Les données présentes sont déjà à jour.\nPas de nouvelles données à télécharger.")


        repertoire = dir_path + constantes.dir_csv
        repertoire_zip = dir_path + constantes.dir_zip
        dirname = tkFileDialog.askdirectory(initialdir=repertoire,title='Please select a directory')
        self.top = Top(self.parent,constantes.img_02)
        self.top.update()
        if dirname:
            chargement(dirname,repertoire_zip)
        self.top.destroy()

class Top(Toplevel):
    def __init__(self, parent, image):
        Toplevel.__init__(self)
        self.parent = parent
        self.overrideredirect(1)
        w = 450 # width top
        h = 450 # height top
        ws = self.winfo_screenwidth() # width screen
        hs = self.winfo_screenheight() # height screen
        x = (ws/2) - (w/2)
        y = (hs/2) - (h/2)
        self.geometry('%dx%d+%d+%d' % (w, h, x, y))
        self.title("Patience...")
        canvas = Canvas(self, width = w, height = h)
        self.img = PhotoImage(file = image)
        label = Label(self, image=self.img, compound=CENTER).pack()
        canvas.pack()

class Top_JSON(Toplevel):
    def __init__(self, parent, text):
        Toplevel.__init__(self)
        self.parent = parent
        if "nt" == os.name:
            root.wm_iconbitmap(bitmap = "img/dataradio.ico")
        else:
            root.wm_iconbitmap(bitmap = "@img/dataradio.xbm")
        frame = Frame(self,borderwidth=1, relief="sunken", width = 600, height = 600)
        self.texte = Text(frame, wrap = NONE, borderwidth=0)
        vsb = Scrollbar(frame, orient=VERTICAL, command=self.texte.yview)
        self.texte.insert('1.0', text)
        self.texte.config(yscrollcommand=vsb.set, state=DISABLED)
        self.texte.grid(row=0, column=0, sticky="nsew")
        self.button = Button(frame, text="Copier le texte dans le presse-papier", command=self.clip)
        self.button.grid(row=1, column=0)
        vsb.grid(row=0, column=1,sticky="nsew",ipady = 100)
        frame.grid(row=0, column=0, rowspan=1, columnspan=1, sticky="nsew")
        frame.columnconfigure(0, weight=1)
        frame.columnconfigure(1, weight=0)
        frame.rowconfigure(0, weight=1)
        frame.rowconfigure(1, weight=0)
        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)
    def clip(self):
        thetext = self.texte.get('1.0', 'end')
        self.clipboard_clear()
        self.clipboard_append(thetext)

dir_path = os.path.dirname(os.path.realpath(__file__))
dictionnaire = {}

if __name__ == '__main__':
    root = Tk()
    app = App(root)
    if "nt" == os.name:
        root.wm_iconbitmap(bitmap = "img/dataradio.ico")
    else:
        root.wm_iconbitmap(bitmap = "@img/dataradio.xbm")
    root.mainloop()
