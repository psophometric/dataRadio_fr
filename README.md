# Description
**dataRadio_fr** est un utilitaire réalisé en langage [Python](https://www.python.org/) qui permet de naviguer dans le jeu de données de l'Anfr [Les installations radioélectriques de plus de 5 watts](https://www.data.gouv.fr/fr/datasets/donnees-sur-les-installations-radioelectriques-de-plus-de-5-watts-1/).

Parmis les fonctionnalités :
* Recherche de support par technologie, exploitant, département
* Recherche de support par identifiant (n° support, station, COMSIS)
* Recherche de mutualisation des sites GSM (support, antenne)
* Recherche d'interférences potentielles entre GSM et GSM-R (nécessite le module *Geopy*)
* Export sur carte OpenStreetMap (nécessite le module *Folium*)
* Export de fichiers au format JSON.

**[Plus d'informations sur le Wiki](https://framagit.org/psophometric/dataRadio_fr/wikis/home)**

# Dépendances
Les dépendances sont optionnelles.
* [Folium](https://github.com/python-visualization/folium) permet l'export des résultats sur une carte OpenStreetMap.
* [Geopy](https://github.com/geopy/geopy) permet de calculer les interférences potentielles entre GSM et GSM-R.

# Installation
* [Linux Debian / Ubuntu](https://framagit.org/psophometric/dataRadio_fr/wikis/installation-linux)
* [Windows](https://framagit.org/psophometric/dataRadio_fr/wikis/installation-windows)
